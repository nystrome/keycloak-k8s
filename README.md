
# Setup

The following passwords need to be added to the Kubernetes cluster.

## Installing kind

See https://gitlab.com/nystrome/kind-tools

```bash
bash <(curl -s https://gitlab.com/nystrome/kind-tools/-/raw/main/install.sh)
```

## Creating secrets

```bash
kubectl create secret generic keycloak \
  --from-literal=admin-user=admin \
  --from-literal=admin-password='changeme'

kubectl create secret generic keycloak-database \
  --from-literal=root-password='changeme' \
  --from-literal=user=keycloak \
  --from-literal=user-password='changeme' \
  --from-literal=backup-user=maria-galera \
  --from-literal=backup-user-password='changeme' \
  --from-literal=database-schema=keycloak
```